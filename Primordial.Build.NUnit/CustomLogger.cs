using System;
using Microsoft.Build.Framework;

namespace Primordial.Build.NUnit
{
    public class CustomLogger : INodeLogger
    {
        private readonly Action<string, string> _logAction;
        private int numberOfProcessors;

        public CustomLogger(Action<string, string> logAction, LoggerVerbosity verbosity = LoggerVerbosity.Normal)
        {
            _logAction = logAction;
            numberOfProcessors = 1;
            Verbosity = verbosity;
        }
        
        #region INodeLogger Members

        public LoggerVerbosity Verbosity { get; set; }

        public string Parameters { get; set; }

        public void Shutdown()
        {
        }

        public void Initialize(IEventSource eventSource, int nodeCount)
        {
            numberOfProcessors = nodeCount;
            Initialize(eventSource);
        }

        public void Initialize(IEventSource eventSource)
        {
            if (eventSource == null) return;
            eventSource.BuildStarted += BuildStartedHandler;
            eventSource.BuildFinished += BuildFinishedHandler;
            eventSource.ProjectStarted += ProjectStartedHandler;
            eventSource.ProjectFinished += ProjectFinishedHandler;
            eventSource.TargetStarted += TargetStartedHandler;
            eventSource.TargetFinished += TargetFinishedHandler;
            eventSource.TaskStarted += TaskStartedHandler;
            eventSource.TaskFinished += TaskFinishedHandler;
            eventSource.ErrorRaised += ErrorHandler;
            eventSource.WarningRaised += WarningHandler;
            eventSource.MessageRaised += MessageHandler;
            eventSource.CustomEventRaised += CustomEventHandler;
        }

        #endregion

        private void BuildStartedHandler(object sender, BuildStartedEventArgs e)
        {
            //base.buildStarted = e.Timestamp;
            //if (IsVerbosityAtLeast(LoggerVerbosity.Normal))
            WriteLog("BuildStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void BuildFinishedHandler(object sender, BuildFinishedEventArgs e)
        {
            WriteLog("BuildFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void ProjectStartedHandler(object sender, ProjectStartedEventArgs e)
        {
            WriteLog("ProjectStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void ProjectFinishedHandler(object sender, ProjectFinishedEventArgs e)
        {
            WriteLog("ProjectFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TargetStartedHandler(object sender, TargetStartedEventArgs e)
        {
            WriteLog("TargetStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TargetFinishedHandler(object sender, TargetFinishedEventArgs e)
        {
            WriteLog("TargetFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TaskStartedHandler(object sender, TaskStartedEventArgs e)
        {
            WriteLog("TaskStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TaskFinishedHandler(object sender, TaskFinishedEventArgs e)
        {
            WriteLog("TaskFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void ErrorHandler(object sender, BuildErrorEventArgs e)
        {
            WriteLog("ErrorRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void WarningHandler(object sender, BuildWarningEventArgs e)
        {
            WriteLog("WarningRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void MessageHandler(object sender, BuildMessageEventArgs e)
        {
            WriteLog("MessageRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void CustomEventHandler(object sender, CustomBuildEventArgs e)
        {
            WriteLog("CustomEventRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private bool IsVerbosityAtLeast(LoggerVerbosity checkVerbosity)
        {
            return (Verbosity >= checkVerbosity);
        }

        private void WriteLog(string action, string format, params object[] arg)
        {
            _logAction(action, string.Format(format, arg));
        }
    }
}