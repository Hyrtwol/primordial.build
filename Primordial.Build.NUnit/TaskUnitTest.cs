﻿using System;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Utilities;
using NUnit.Framework;

namespace Primordial.Build.NUnit
{
    public abstract class TaskUnitTest<T> : BuildEngineTest
        where T : Task
    {
#pragma warning disable 0618
        private Project _project;
        protected Project TestProject { get { return _project; } }
#pragma warning restore 0618

        private Target _testTarget;
        private BuildTask _testTask;

        protected Target TestTarget { get { return _testTarget; } }
        protected BuildTask TestTask { get { return _testTask; } }

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            _project = Engine.CreateNewProject();
            _project.DefaultTargets = "Test";
            
            SetUpTarget();
        }

        protected virtual void SetUpTarget()
        {
            var taskType = typeof (T);
            _project.AddNewUsingTaskFromAssemblyName(taskType.Name, taskType.Assembly.FullName);
            if (_project.Targets == null) throw new NullReferenceException();
            _testTarget = _project.Targets.AddNewTarget("Test");
            SetupTask(taskType);
        }

        protected virtual void SetupTask(Type taskType)
        {
            _testTask = _testTarget.AddNewTask(taskType.Name);
        }

        [TearDown]
        public override void TearDown()
        {
            _project = null;
            //if (Directory.Exists(outputDirectory))
            //{
            //    Debug.Print("deleting directory \"{0}\"", outputDirectory);
            //    Directory.Delete(outputDirectory, true);
            //}
            base.TearDown();
        }

        protected void WriteProjectXml()
        {
            WriteProjectXml(_project);
        }

        protected void WriteProjectXml(string fileName)
        {
            WriteProjectXml(_project, fileName);
        }

        protected void AssertProjectBuild(bool expectedResult = true)
        {
            AssertProjectBuild(_project, expectedResult);
        }
    }
}