using System.Diagnostics;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Framework;
using NUnit.Framework;

namespace Primordial.Build.NUnit
{
    public abstract class BuildEngineTest
    {
#pragma warning disable 0618

        protected Engine Engine { get; private set; }
        protected string DefaultTargets { get; set; }
        protected string DefaultToolsVersion { get; set; }
        protected LoggerVerbosity Verbosity { get; set; }

        protected BuildEngineTest()
        {
            DefaultToolsVersion = "4.0";
            Verbosity = LoggerVerbosity.Normal;
        }

        protected virtual void LogLine(string format, params object[] arg)
        {
        }

        [SetUp]
        public virtual void SetUp()
        {
            LogLine("Engine SetUp");
            Engine = new Engine {DefaultToolsVersion = DefaultToolsVersion};
            WriteEngineVersions();
            RegisterEngineLoggers();
        }

        [TearDown]
        public virtual void TearDown()
        {
            LogLine("Engine TearDown");
            Engine.UnloadAllProjects();
            Engine.UnregisterAllLoggers();
            Engine.Shutdown();
            Engine = null;
        }

        protected virtual void RegisterEngineLoggers()
        {
            var logger = new ConsoleLogger();
            logger.Verbosity = Verbosity;
            Engine.RegisterLogger(logger);
        }

        [Conditional("DEBUG")]
        protected void WriteEngineVersions()
        {
            LogLine("Engine.Version={0}", Engine.Version);
            LogLine("Engine.DefaultToolsVersion={0}", Engine.DefaultToolsVersion);
        }
        
        protected void AssertProjectBuild(Project project, bool expectedResult = true)
        {
            //LogLine("-- Project Build ---------------------------------------------------------------");            
            Assert.AreEqual(expectedResult, project.Build(DefaultTargets), "MSBuild failed.");
            //LogLine("--------------------------------------------------------------------------------");
        }

        protected void WriteProjectXml(Project project)
        {
            LogLine("-- Project Xml -----------------------------------------------------------------");
            LogLine(project.Xml);
            LogLine("--------------------------------------------------------------------------------");
        }

        protected void WriteProjectXml(Project project, string path)
        {
            WriteProjectXml(project);
            project.Save(path);
        }

        protected void WriteAndBuildProject(Project project, bool expectedResult)
        {
            WriteProjectXml(project);
            AssertProjectBuild(project, expectedResult);
        }

#pragma warning restore 0618
    }
}