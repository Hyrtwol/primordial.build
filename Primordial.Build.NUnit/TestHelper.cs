﻿using System.IO;
using NUnit.Framework;

namespace Primordial.Build.NUnit
{
    public static class TestHelper
    {
        public static void AssertFileExists(string path)
        {
            Assert.IsTrue(File.Exists(path),
                string.Format("File \"{0}\" does not exists.", path));
        }

        public static void AssertDirectoryExists(string path)
        {
            Assert.IsTrue(Directory.Exists(path),
                string.Format("Directory \"{0}\"  does not exists.", path));
        }

        public static void AssertTextFileContent(string expectedContent, string fileName)
        {
            AssertFileExists(fileName);
            var actual = File.ReadAllText(fileName);
            Assert.AreEqual(expectedContent, actual);
        }
    }
}
