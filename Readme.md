#Primordial.Build

Utilities and extensions for Microsoft.Build

* [Microsoft.Build Namespaces](https://msdn.microsoft.com/en-us/library/gg145008(v=vs.110).aspx)

## Primordial.Build

Microsoft.Build extensions.

## Primordial.Build.NUnit

NUnit extensions for testing ```Microsoft.Build.Utilities.Task```

## Primordial.Build.NLog (TODO)

NLog implementation of ```Microsoft.Build.Framework.INodeLogger```

## Primordial.Build.log4net (TODO)

log4net implementation of ```Microsoft.Build.Framework.INodeLogger```
