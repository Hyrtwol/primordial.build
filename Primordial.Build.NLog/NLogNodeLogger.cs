﻿using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Primordial.Build.NLog
{
    public class NLogNodeLogger : INodeLogger
    {
        // ReSharper disable NotAccessedField.Local
        private int _numberOfProcessors;
        // ReSharper restore NotAccessedField.Local

        public LoggerVerbosity Verbosity { get; set; }
        public string Parameters { get; set; }

        public void Initialize(IEventSource eventSource, int nodeCount)
        {
            InitializeLogger(eventSource, nodeCount);
        }

        public void Initialize(IEventSource eventSource)
        {
            InitializeLogger(eventSource, 1);
        }

        private void InitializeLogger(IEventSource eventSource, int nodeCount)
        {
            _numberOfProcessors = nodeCount;

            if (eventSource == null) return;

            //this.ParseFileLoggerParameters();

            //Debug.Print("Create ELS Client {0}", _elasticsearchUrl);
            //if (_elsClient != null) throw new InvalidOperationException("Redis ElasticsearchClient already initialized.");
            //var elsuri = new Uri(_elasticsearchUrl);
            //_elsClient = new ElasticsearchClient<BuildLogEvent>(elsuri);
            //if (!string.IsNullOrEmpty(_indexName)) _elsClient.IndexName = _indexName;
            //if (!string.IsNullOrEmpty(_indexType)) _elsClient.IndexType = _indexType;

            eventSource.BuildStarted += LogToELS;
            eventSource.BuildFinished += LogToELS;
            //eventSource.ProjectStarted += ProjectStartedHandler;
            //eventSource.ProjectFinished += ProjectFinishedHandler;
            //eventSource.TargetStarted += TargetStartedHandler;
            //eventSource.TargetFinished += TargetFinishedHandler;
            //eventSource.TaskStarted += TaskStartedHandler;
            //eventSource.TaskFinished += TaskFinishedHandler;
            eventSource.ErrorRaised += LogToELS;
            eventSource.WarningRaised += LogToELS;
            eventSource.MessageRaised += LogToELS;
            //eventSource.CustomEventRaised += CustomEventHandler;
        }

        private void LogToELS(object sender, BuildEventArgs e)
        {
#if DEBUG
            Debug.WriteLine(e.Message);
#endif
            //if (_elsClient == null) return;
            //var logEvent = CreateLogEvent(e);
            //if (logEvent == null) return;
            //_elsClient.SendMessage(logEvent);
        }

        public void Shutdown()
        {
#if DEBUG
            Debug.WriteLine(nameof(NLogNodeLogger)+ ".Shutdown()");
#endif
        }
    }
}
