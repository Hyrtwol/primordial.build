﻿using Microsoft.Build.Utilities;

namespace Primordial.Build
{
    public static class CommandLineBuilderExtensions
    {
        public static void AppendBoolSwitch(this CommandLineBuilder builder, string boolStr, string switchStr)
        {
            if (string.IsNullOrEmpty(boolStr)) return;
            if (bool.TryParse(boolStr, out var generateHeaderFile) && generateHeaderFile)
                builder.AppendSwitch(switchStr);
        }
    }
}