﻿using Microsoft.Build.BuildEngine;
using Microsoft.Build.Utilities;

namespace Primordial.Build
{
    public static class ProjectExtensions
    {
#pragma warning disable 0618
        public static void AddNewUsingTaskFromAssemblyName<T>(this Project project) where T : Task
        {
            var taskType = typeof(T);
            project.AddNewUsingTaskFromAssemblyName(taskType.Name, taskType.Assembly.FullName);
        }
#pragma warning restore 0618
    }
}