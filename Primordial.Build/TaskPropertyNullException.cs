﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Primordial.Build
{
    [Serializable, ComVisible(true)]
    public class TaskPropertyNullException : ApplicationException
    {
        public string PropertyName { get; }

        public TaskPropertyNullException()
            : base("Task property is null or empty.")
        {
        }

        public TaskPropertyNullException(string propertyName)
            : base($"Task property {propertyName} is null or empty.")
        {
            PropertyName = propertyName;
        }

        public TaskPropertyNullException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public TaskPropertyNullException(string propertyName, string message)
            : base(message)
        {
            PropertyName = propertyName;
        }

        public TaskPropertyNullException(string message, string propertyName, Exception innerException)
            : base(message, innerException)
        {
            PropertyName = propertyName;
        }

        protected TaskPropertyNullException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            PropertyName = info.GetString("PropertyName");
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }
            base.GetObjectData(info, context);
            info.AddValue("PropertyName", PropertyName, typeof(string));
        }
    }
}