﻿using System;
using Microsoft.Build.BuildEngine;

namespace Primordial.Build
{
    public static class BuildTaskExtensions
    {
        public static void SetParameterValue(this BuildTask task, string parameterName, bool parameterValue)
        {
            if (string.IsNullOrEmpty(parameterName)) throw new ArgumentNullException(nameof(parameterName));
            task.SetParameterValue(parameterName, parameterValue.ToString());
        }
    }
}