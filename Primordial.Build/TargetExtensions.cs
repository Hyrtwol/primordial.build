﻿using Microsoft.Build.BuildEngine;
using Microsoft.Build.Utilities;

namespace Primordial.Build
{
    public static class TargetExtensions
    {
        public static BuildTask AddMessageTask(this Target target, string message, string importance = "high")
        {
            var task = target.AddNewTask("Message");
            task.SetParameterValue("Text", message);
            task.SetParameterValue("Importance", importance);
            return task;
        }

        public static BuildTask AddNewTask<T>(this Target target) where T: Task
        {
            var typeOfT = typeof (T);
            var task = target.AddNewTask(typeOfT.Name);
            return task;
        }
    }
}