﻿using Microsoft.Build.Utilities;

namespace Primordial.Build
{
    static class LogExtensions
    {
        public static void LogKeyValue(this TaskLoggingHelper log, string key, object value)
        {
            log.LogMessage("{0,-20} \"{1}\"", key+":", value);
        }
    }
}