﻿//
// Copyright 2016 David Roller
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
using Microsoft.Build.Framework;
using System;
using System.IO;

namespace MSBuildLogger
{
    //public class MarkdownLogger : Microsoft.Build.Utilities.Logger
    //{
    //}

    public class HTMLLogger : Microsoft.Build.Utilities.Logger
    {
        private FileInfo file = null;

        public override void Initialize(IEventSource eventSource)
        {
            eventSource.BuildStarted += eventSource_BuildStarted;
            eventSource.WarningRaised += eventSource_WarningRaised;
            eventSource.ErrorRaised += eventSource_ErrorRaised;
            eventSource.BuildFinished += eventSource_BuildFinished;
        }

        //triggered when build started
        private void eventSource_BuildStarted(Object sender, BuildStartedEventArgs e)
        {
            DirectoryInfo dirInf = new DirectoryInfo("BuildLogging");
            if (dirInf.Exists == false)
            {
                dirInf.Create();
            }

            file = new FileInfo("BuildLogging/ErrorWarnings_" + DateTime.Now.ToString("MMddyy_HHmmss") + ".html");
            if (file.Exists)
            {
                file.Delete();
            }

            using (var stream = new StreamWriter(file.FullName, true))
            {
                stream.WriteLine("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
                stream.WriteLine("<html><head>");
                stream.WriteLine("<title>Build Result</title>");
                stream.WriteLine("<meta charset=\"utf-8\" />");
                stream.WriteLine("<style type=\"text/css\">");
                stream.WriteLine("table tr td{");
                stream.WriteLine("font-size:8px;");
                stream.WriteLine("font-family:Verdana, Arial, Helvetica, sans-serif;");
                stream.WriteLine("}");
                stream.WriteLine("</style>");
                stream.WriteLine("</head>");
                stream.WriteLine("<body>");
                stream.WriteLine("<table border=\"1\">");
                stream.WriteLine("<tr><th>Time</th><th>Type</th><th>Project</th><th>Message</th></tr>");
                stream.WriteLine("<tr><td>" + DateTime.Now.ToString() + "</td><td>Info</td><td></td><td>" + e.Message + "</td></tr>");
            }
        }

        //triggered when a warning is encountered
        private void eventSource_WarningRaised(object sender, BuildWarningEventArgs e)
        {
            using (var stream = new StreamWriter(file.FullName, true))
            {
                stream.WriteLine("<tr bgcolor=\"#FFFF00\"><td>" + e.Timestamp.ToString() + "</td><td>Warning</td><td>" + e.ProjectFile + "</td><td>" + e.Message + "</td></tr>");
            }
        }

        //triggered when an error is encountered
        private void eventSource_ErrorRaised(Object sender, BuildErrorEventArgs e)
        {
            using (var stream = new StreamWriter(file.FullName, true))
            {
                stream.WriteLine("<tr bgcolor=\"#FF6600\"><td>" + e.Timestamp.ToString() + "</td><td>Error</td><td>" + e.ProjectFile + "</td><td>" + e.Message + "</td></tr>");
            }
        }

        //triggered when the compiling process is over
        private void eventSource_BuildFinished(object sender, BuildFinishedEventArgs e)
        {
            using (var stream = new StreamWriter(file.FullName, true))
            {
                stream.WriteLine("<tr><td>" + e.Timestamp.ToString() + "</td><td>Info</td><td></td><td>" + e.Message + "</td></tr>");

                stream.WriteLine("</table>");
                stream.WriteLine("</body>");
                stream.WriteLine("</html>");
            }
        }
    }
}
