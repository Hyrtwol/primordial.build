﻿using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Primordial.Build
{
    public abstract class TaskBase : Task
    {
        public override bool Execute()
        {
            try
            {
                ExecuteTask();
            }
            catch (TaskPropertyNullException ex)
            {
                Log.LogError("Task property {0} is null or empty.", ex.PropertyName);
            }
            catch (Exception ex)
            {
                Log.LogErrorFromException(ex, true);
            }
            return !Log.HasLoggedErrors;
        }

        protected abstract void ExecuteTask();

        protected void LogTask()
        {
            Log.LogMessage(MessageImportance.High, "Executing task {0}", GetType().AssemblyQualifiedName);
        }
    }
}