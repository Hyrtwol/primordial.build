﻿using System;
using System.Collections.Generic;
using Microsoft.Build.Utilities;

namespace Primordial.Build
{
    public static class TaskItemExtensions
    {
        public static void SetMetadata(this TaskItem taskItem, string metadataName, bool metadataValue)
        {
            if (string.IsNullOrEmpty(metadataName)) throw new ArgumentNullException(nameof(metadataName));
            taskItem.SetMetadata(metadataName, metadataValue.ToString());
        }

        public static void SetMetadataIsNotNull(this TaskItem taskItem, string metadataName, string metadataValue)
        {
            if (string.IsNullOrEmpty(metadataName)) throw new ArgumentNullException(nameof(metadataName));
            if (string.IsNullOrEmpty(metadataValue)) return;
            taskItem.SetMetadata(metadataName, metadataValue);
        }

        public static void SetMetadata(this TaskItem taskItem, string metadataName, IEnumerable<string> metadataValues)
        {
            if (string.IsNullOrEmpty(metadataName)) throw new ArgumentNullException(nameof(metadataName));
            if(metadataValues==null) return;
            var metadataValue = string.Join(";", metadataValues);
            if (string.IsNullOrEmpty(metadataValue)) return;
            taskItem.SetMetadata(metadataName, metadataValue);
        }
    }
}